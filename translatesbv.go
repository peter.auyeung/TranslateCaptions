package main

import (
    "bufio"
    "fmt"
    "log"
    "os"
    "unicode"

    // Imports the Google Cloud Translate client package.
    "cloud.google.com/go/translate"
    "golang.org/x/net/context"
    "golang.org/x/text/language"
)

func gotranslate(text string) string {

    ctx := context.Background()
    // Creates a client.
    client, err := translate.NewClient(ctx)
    if err != nil {
        log.Fatalf("Failed to create client: %v", err)
    }
    // Sets the target language.
    target, err := language.Parse("zh-TW")
    if err != nil {
        log.Fatalf("Failed to parse target language: %v", err)
    }
    // Translates the text into Language we need.
    translations, err := client.Translate(ctx, []string{text}, target, nil)
    if err != nil {
        log.Fatalf("Failed to translate text: %v", err)
    }
return translations[0].Text
}

func main() {
    caption := os.Args[1]
    f, err := os.Open(caption) // just pass the file name
    if err != nil {
        fmt.Print(err)
    }

    Scanner := bufio.NewScanner(f)
    for Scanner.Scan() {
        line := []rune(Scanner.Text())
        if len(line) > 0 && unicode.IsLetter(line[0]) {
            fmt.Printf("%v\n", gotranslate(Scanner.Text()))
        } else {
            fmt.Printf("%s\n", Scanner.Text())
        }
    }
}
