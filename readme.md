**Translate video caption**

Modules: \
% go mod init translatesbv \
% go get cloud.google.com/go/translate

Environment: \
% export GOOGLE_APPLICATION_CREDENTIALS="service_account.json"

Usage: \
% go run translatesbv caption.sbv
